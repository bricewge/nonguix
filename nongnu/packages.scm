;;; Copyright © 2021 Brice Waegeneire <brice@waegenei.re>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (nongnu packages)
  #:use-module (gnu packages)
  #:export (search-patch-nonguix
            search-patches-nonguix))

;; NOTE Guix should be enhanced to definie a patches directory on a channel by
;; channel basis.

(define (search-patch-nonguix file-name)
  "Like 'search-patch', but also search patch FILE-NAME in Nonguix repository
in addition to Guix."
  (parameterize
      ((%patch-path
        (map (lambda (directory)
               (string-append directory "/nongnu/packages/patches"))
             %load-path)))
    (search-patch file-name)))

(define-syntax-rule (search-patches-nonguix file-name ...)
"Like 'search-patches', but also search in Nonguix repository in addition to
Guix."
  (list (search-patch-nonguix file-name) ...))

;; TODO Provide similar procedure for aux-files.
